<?php
/**
 * @package FP_SMS_Password_Reset
 * @version 1.0
 */
/*
Plugin Name: FP SMS Password Reset
Plugin URI: http://www.forepoint.co.uk
Description: Overrides the default password recovery system to instead generate, and then send the user their new password via SMS (using Bulk SMS)
Author: Dale Webster
Version: 1.0
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// ------------------------------------------------------------------------

// Display phone number field in user profile section

// ------------------------------------------------------------------------

add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

// ------------------------------------------------------------------------

// Save the phone number

// ------------------------------------------------------------------------

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

// ------------------------------------------------------------------------]

// ------------------------------------------------------------------------

function my_show_extra_profile_fields ( $user ) 
{ 
	?>

	<h3>Security Information</h3>

	<table class="form-table">

		<tr>
			<th><label for="phone">Phone Number</label></th>

			<td>
				<span>
					+
				</span>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">
					Please enter your phone number, including the region code (EG. 44 instead of 0).
					<br />
					In the event that you forget your password, a new password will be sent to this 
					number via SMS.
				</span>
			</td>
		</tr>

	</table>
	<?php 
}

function my_save_extra_profile_fields( $user_id ) 
{
	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_usermeta( $user_id, 'phone', $_POST['phone'] );
}

// ------------------------------------------------------------------------

// Create Config Menu

if ( is_admin() )
{
	add_action( "admin_init" , function()
		{	
			register_setting( 'fp-pass-reset' , 'bulk-sms-username' );
			register_setting( 'fp-pass-reset' , 'bulk-sms-password' );
		} );
	add_action( "admin_menu" , function()
		{
			add_options_page( "FP SMS Password Reset" , "FP SMS Password Reset" , "manage_options" , "fp-pass-reset" , function()
				{
					if ( ! current_user_can( 'manage_options' ) )  
					{
						wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
					}

					echo "<div class = 'wrap' >";

					echo "<form method='post' action='options.php'> ";

					settings_fields( 'fp-pass-reset' );
					do_settings_sections( 'fp-pass-reset' );

					?>
					<table class = 'form-table'>
						<h2>
							FP SMS Password Reset
						</h2>
						<hr>
						<p>
							Bulk SMS Account Details
						</p>
						<hr>
						<tr valign="top">
							<th scope="row">Bulk SMS Username</th>
							<td>
								<input type="text" name="bulk-sms-username" value="<?php echo esc_attr( get_option('bulk-sms-username') ); ?>" />
							</td>
						</tr>

						<tr valign="top">
							<th scope="row">Bulk SMS Password</th>
							<td>
								<input type="password" name="bulk-sms-password" value="password" />
							</td>
						</tr>

					</table>
					<?php
					
					submit_button();

					echo "</form>";

					echo "</div>";

				} );
		} );	
}

// Encrypt the bulk sms password upon submission
add_filter( "pre_update_option_bulk-sms-password" , function( $new , $old )
	{
		return fp_encrypt( $new , AUTH_KEY );
	} , 10 , 2 );

add_action( "lostpassword_post" , function( $user  )
	{
		// Check that the details for Bulk SMS have been provided. There will be issues later
		// if not
		if ( ! get_option( "bulk-sms-username" )  || ! get_option( "bulk-sms-password" ) )
		{
			// If we return false, the default behaviour should take over.
			return FALSE;
		}

		// Get the user data
		if ( isset( $_POST[ "user_login" ] ) )
		{
			// Get the user information
			$user = get_user_by( "login" , $_POST[ "user_login" ] );

			if ( ! $user ) wp_die( "Couldn't find the user" );

			// Get the user's phone number
			$phone = get_user_meta( $user->ID , "phone" , TRUE );

			if ( ! $phone ) wp_die( "No phone number attached to the user's account" );

			// Email the user a password reset notice
			email_password_reset_notice( $user->ID , $phone );

		}
		else
		{
			wp_die( "No Username found" );
		}

	} , 10 );

function email_password_reset_notice ( $user_id , $phone_number  )
{	
	// Get the user
	$user = get_user_by( "id" , $user_id );
	$email_address = $user->data->user_email;

	// Create a key by encrypting the user id
	$key = fp_encryot_int( $user_id );
	$href = get_site_url() . "/wp-content/plugins/fp-password-reset/send-sms.php?user=" . $key;

	$subject = "Request to reset password";
	$message = "A password reset has been requested for the account attached to this email address. <br />";
	$message .= "If you have requested to have your password reset, follow the link below and your new password will be sent ";
	$message .= "via SMS to $phone_number. <br /> <br /> <br />";
	$message .= "<a href = '$href' >$href</a>  ";

	$headers = "Content-type: text/html";

	if ( wp_mail( $email_address , $subject , $message , $headers ) )
	{
		// wp_die( "The email was sent successfully" );
		fp_success_page();
	}
	else
	{
		wp_die( "A problem was encountered while trying to send the email." );
	}

}

// ------------------------------------------------------------------------
	
// Misc Functions

// ------------------------------------------------------------------------

function fp_success_page ()
{
	require "public/email_success.php";
	die();
}

function fp_encrypt( $input_string , $key )
{
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $h_key = hash('sha256', $key, TRUE);
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $h_key, $input_string, MCRYPT_MODE_ECB, $iv));
}

function fp_decrypt( $encrypted_input_string , $key )
{
    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $h_key = hash('sha256', $key, TRUE);
    return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $h_key, base64_decode($encrypted_input_string), MCRYPT_MODE_ECB, $iv));
}

function fp_encryot_int ( $int )
{	
	return $int * (int) date( "md" );
}

function fp_decrypt_int ( $int )
{
	return $int / (int) date( "md" );
}