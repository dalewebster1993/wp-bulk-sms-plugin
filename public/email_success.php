<html>
<head>
	<meta charset="UTF-8">
	<title>Password Reset</title>
</head>
<style>
	body {
		background-color: #f1f1f1;
		font-family: "Open Sans";
	}
	a {
		color: #0074a2;
	}
	.box {
		width: 30%;
		margin: 50px auto;
		text-align: center;
		background-color: white;
		padding: 50px;
		color: #777;
		box-shadow: 0 1px 3px rgba(0,0,0,.13);
	}
</style>
<body>
	<div class="wrap">
		<div class="box">
			<p class="message">
				Check your email for further instructions on resetting your password.
			</p>
			<p>
				Click <a href = "<?php echo wp_login_url() ?>" > here </a> to return to login </div>
			</p>
		</div>	
	</div>
</body>
</html>